---
title: Grafik Apps
subtitle: Zeichentools
comments: false
---

## Plant PlantUML

[Plant UML](https://plantuml.com/de/)

Für Visual Studio Code gibt es ein Plugin "PlantUML" in welchem der Code geschrieben werden kann und dann mit [Alt] + [D] die Vorschau geöffnet werden kann.

## Mermaid

Ein anderer online Grafikeditor
[Mermaid](https://mermaid-js.github.io/mermaid/#/)

## Diagrams Net

Online Zeichenprogramm: [diagrams.net](https://www.diagrams.net/)
