---
title: Visual Studio
subtitle: Cheatsheet Visual Studio
comments: false
---

## Visual Studio Cheatsheet

| Tastencode | Funktion |
|-----|---|
| `[Alt]+[Z]` |  Zeilenumbruch bei langen Zeilen |
| `[Ctrl]+[K] [V]` | Vorschau für Markdown Dokumente |  

## WSL2 Tipps

VS Code aus der WSL2 Shell starten mit `code .`

## Mac Tipps

VS Code aus der shell öffnen mit `code myproject.code-workspace` oder `code`
[VS Infos zu Mac von Microsoft](https://code.visualstudio.com/docs/setup/mac)
