---
title: tmux 
subtitle: Tipps und Tricks tmux
comments: false
tags: [ "tmux","linux","tipps"]
---


## Tmux Benutzen und einrichten

### Bedienung tmux

```text
[Ctrl]+[X] :
selectl even-vertical
```

### Einstellungen für tmux

Umstellen von Ctrl-B  auf Ctrl-X als Hotkey
Zeiichen - für horizontale Teilung
Zeichen | für vertikale Teilung

```vim
# vim ~/.tmux.conf
# Ctrl-X 
set-option -g prefix C-x
 
# split panes using | and -
bind / split-window -h
bind - split-window -v
unbind '"'
unbind %
```
