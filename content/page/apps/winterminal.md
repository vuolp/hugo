---
title: Windows Terminal
subtitle: Cheatsheet Windows Terminal
comments: false
---


## Windows Terminal Cheatsheet

### Bedienung

[Alt]+[Shift]+[+] Vertikales Pane

[Alt]+[Shift]+[-] Horizontales Pane

### Settings

Link zu Microsoft Doku: [Settings]( https://docs.microsoft.com/en-us/windows/terminal/customize-settings/startup)

### Kopieren beim Markieren

Kopieren beim Markieren, wie bei Putty. Nach dem Defaultprofil und vor den Profilen einfügen:
`"copyOnSelect": true,`
