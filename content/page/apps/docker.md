---
title: Docker
subtitle: Cheatsheet Docker
comments: false
tags: [ "docker","log"]
---

## Docker Log anzeigen

Ausgabe der Logfiles mittels docker compose

```bash
docker compose logs -f
```
