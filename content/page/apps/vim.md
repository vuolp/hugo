---
title: vim 
subtitle: Tipps und Tricks vim
comments: false
tags: ["linux", "vim"]
---

# Suchen und Ersetzen

`%s/Suchbegriff/Ersetzung/g`

Das Prozentzeichen zu Beginn zeigt an, dass sich der Befehl auf die ganze Datei bezieht. 
Das g am Ende steht für "global" und bedeutet, dass Vim den String auch ersetzt, wenn er in einer Zeile mehrmals vorkommt. 
In der Praxis werden meist diese beiden Optionen verwendet.
 
Den Grundbefehl können Sie auf mehrfache Weise variieren. Lassen Sie das Prozentzeichen weg, sucht Vim nur in der aktuellen Zeile. Ersetzen Sie es durch einen Bereich, etwa 4,6,, sucht Vim nur in den Zeilen 4 bis 6:
`4,6,s/Suchbegriff/Ersetzung/g`


## Mehrer Zeilen auskommentieren

- Cursor zur ersten Zeile bewegen.
- \[V\] drücken
- mit der Pfeiltaste bis zur letzten Zeile fahren.

```bash
:s/^/#/
```

- mit \[Enter\] bestätigen

## Farbdarstellung ändern

```bash
vim ~/.vimrc

"set number     " Showing Line Numbers

syntax on       " Enable Syntax Highlighting

set background=dark

set colorscheme=murphy     " blue, darkblue, delek, desert, elford, evening, industry, koehler, morning, murphy, pablo, peachpuff, ron, shine, slate, torte, zellner

set tabstop=4       "Setting Tab Size

set softtabstop=4   " number of spaces in tab when editing

set colorcolumn=80

set autoindent      " Enabling Automatic Indentation

set expandtab       " Replacing Tabs with White Spaces

set cursorline  " Highlight the Current Line

set incsearch           " search as characters are entered
set hlsearch            " highlight matches

set foldenable          " enable folding

set laststatus=2
```

## Visueller Mode ausschalten

Entwede im Editor direkt oder persistent im File:

```bash
set mouse-=a
vim ~/.vimrc
```
