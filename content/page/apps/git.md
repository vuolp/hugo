---
title: Git 
subtitle: Tipps und Tricks git
comments: false
tags: [ "git"]
---


## git Tipps und Tricks

Lokaler Filename mit Version des Repos überschreiben
git checkout filename

## Lokales File mit Version vom Repo überschreiben

git checkout \<**filename**\>

```bash
git checkout main.py
```
