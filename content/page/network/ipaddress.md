---
title: Network IP Addresses
subtitle: Netzwerkthemen IPv4 Addressierung
comments: false
tags: [ "network"]
---

## IPv4 Netzwerk Adressierung

| Link | Beschreibung |
|---|---|
|  [IANA](https://www.iana.org/) | Internet Assigned Numbers Authority (IANA) |
|  [RIPE Database Query](https://apps.db.ripe.net/db-web-ui/query) |  Abfrage nach dem Eigentümer Europäischer IP-Adressen |
|  [IP Calculator](http://jodies.de/ipcalc?host=192.168.0.1&mask1=24&mask2=) |  IP Calculator |
|  [IP zu Dezimalzahl](http://www.geektools.com/cgi-bin/ipconv.cgi) |  Konvertiert eine IPv4 Adresse in die Dezimalzahl |

## Tools

## Eigene IP Adresse Browser

[https://browserleaks.com/ip](https://browserleaks.com/ip)

[https://www.whatismyip.com/de/](https://www.whatismyip.com/de/)

## Eigene IP Adresse Komandozeile

IPv4 mit dig

```bash
dig -4 TXT +short o-o.myaddr.l.google.com @ns1.google.com
```

IPv6 mit dig

```bash
dig -6 TXT +short o-o.myaddr.l.google.com @ns1.google.com

Mit wget

```bash
wget https://ifconfig.me/ip -qO-
```

Mit curl

```bash
curl ifconfig.me
``
