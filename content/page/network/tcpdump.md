---
title: tcpdump
subtitle: Netzwerkanalyzer tcpdump
comments: false
tags: [ "network","tcpdump","vlan"]
---

## tcpdump

### Anzeigen der Ethernet Frames und der VLAN Tags

```bash
tcpdump -i eth0 -nn -e '(vlan 1006)'
```

### Logische Verknüpungen

```bash
tcpdump -i eth0 -nn 'host 10.20.30.40 and (port 80 or port 443)'
```
