---
title: HTTP Security
subtitle: Security im Security Bereich
comments: false
tags: [ "network"]
---

## SSL Tests

```bash
openssl s_client -connect hostname.com:443 -tls1
openssl s_client -connect hostname.com:443 -tls1_1
openssl s_client -connect hostname.com:443 -tls1_2
```
