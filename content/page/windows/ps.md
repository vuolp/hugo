---
title: Windows Powershell
subtitle: Powershell - Tipps und Tricks
comments: false
tags: [ "PowerShell","windows","tipps"]
---

## Tail Powershell

Anzeige eines Files dass sich ändern.

```powershell
Get-Content filename -Wait -Tail 30
```

## Powershell zu admin wechseln

von einer normalen shell zu einer mit Admin Rechten wechseln

```powershell
powershell Start-Process powershell -Verb runAs
```
