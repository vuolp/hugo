---
title: Windows Netzwerkthemen
subtitle: Alles rund um die Netzwerkthemen mit Windows
comments: false
---

# Verbindungstest Powershell

```
Test-NetConnection -ComputerName 123.123.123.123 -Port 22
```

<a href="https://docs.microsoft.com/en-us/powershell/module/nettcpip/test-netconnection?view=win10-ps" target="_blank">Details von Microsoft</a>

# Tail Powershell
Anzeige eines Files dass sich ändern.
```
Get-Content filename -Wait -Tail 30
```

# grep Powershell
File nach Pattern durchsuchen
```
Select-String -Path "C:\Temp\*.json" -Pattern "gesuchtertext"
```

```
ls | Select-String "suchtext"
```

# Aktive Netzwerkverbindungen auf einem Windowsserver

Anzahl Verbundene Clients auf einem Windowsserver, welche mit einem Port verbunden sind. Mit Select-String kann gefiltert werden.

netstat -na | Select-String -Pattern ':\<PortNummer\>'

```powershell
netstat -na | Select-String -Pattern ':443'
```
