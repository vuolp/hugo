---
title: Ubuntu Systemadministration
subtitle: Ubuntu Systemmanagement Tips und Tricks 
comments: false
tags: ["linux","ubuntu","systemmangement"]
---

## Zeit

### Prüfen der Einstellungen

Abfragen der Einstellungen.

```bash
sudo timedatectl
```

Prüfen der aktuellen Einstellung

```bash
sudo timedatectl timesync-status
```

Beispiel:

```text
root@server:/home/user# timedatectl timesync-status
       Server: 195.176.26.204 (0.ch.pool.ntp.org)
Poll interval: 1min 4s (min: 32s; max 34min 8s)
         Leap: normal
      Version: 4
      Stratum: 1
    Reference: MRS
    Precision: 4us (-18)
Root distance: 167us (max: 5s)
       Offset: -8.200ms
        Delay: 5.731ms
       Jitter: 0
 Packet count: 1
    Frequency: -54.592ppm
```

### Konfiguration der Zeitserver

#### NTP Einstellung

```bash
sudo vim /etc/systemd/timesyncd.conf
```

Beispieleinstellungen

```text
[Time]
NTP=0.ch.pool.ntp.org 1.ch.pool.ntp.org 2.ch.pool.ntp.org 3.ch.pool.ntp.org
FallbackNTP=ntp.ubuntu.com
```

NTP aktiviern

```bash
sudo timedatectl set-ntp false
sudo timedatectl set-ntp true
# Falls Fehlermeldung: Failed to set ntp: NTP not supported
# sudo apt install systemd-timesyncd 
```

#### Zeitzone einstellen

Aufliste aller Zeitzonen:

```bash
sudo timedatectl list-timezones | grep Europe
```

Setzen einer neuen Zeitzone. Besipiel für Zürich:

```bash
sudo timedatectl set-timezone Europe/Zurich
```

### Tastatur und Locales einstellen

Prüfen was konfiguriert ist:

```bash
localectl status 
```

Verfügbare Locales anzeigen:

```bash
locale -a
```

Locale generieren

```bash
sudo locale-gen de_CH.UTF-8
# Falls Fehler: locale-gen: command not found
apt-get install locales
```

```bash
update-locale LANG=de_CH.UTF-8
````
