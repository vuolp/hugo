---
title: Command Line
subtitle: Alles an der Command Line
comments: false
tags: ["linux"]
---

## Dateien anzeigen und verarbeiten

### Konfigfile filtern, so dass auskommentierte Linien nicht ausgegeben werden

Komentarlinien welche mit # oder ; beginnen ausfiltern

```bash
egrep -v "^$|^[[:space:]]*#|^[[:space:]]*#;" myconfigfile.conf
```

### OS Version anzeigen

`cat /etc/os-release`

## Disk

### UUID der Disk auflisten

Anzeige der UUID aller Disk's: `ls -l /dev/disk/by-uuid
