---
title: Linux Netzwerkthemen
subtitle: Alles rund um die Netzwerkthemen mit Linux
comments: false
tags: ["linux","network"]
---

## netcat Vebindungstest

## TCP-Port

nc -z -n -v -w2 \<**IP-Address**\> \<**Port**\>

```bash
nc -z -n -v -w2 10.11.12.13 54321
```

|   |   |   |  
|---|---|---|
|  -n |  --nodns |  Do not resolve hostnames via DNS|
|  -v |  --verbose |  Set verbosity level (can be used several times) |
|  -w |  --wait \<time\> | Connect timeout |  
|  -z |  |  Zero-I/O mode, report connection status only |  

## UDP-Port

nc -n -u -z -v -w2 \<**IP-Address**\> \<**Port**\>

```bash
nc -n -u -z -v -w2 10.11.12.13 54321
```

|   |   |   |  
|---|---|---|
|  -u |   | Use UDP instead of TCP. |  
|  -n |  --nodns |  Do not resolve hostnames via DNS|
|  -v |  --verbose |  Set verbosity level (can be used several times) |
|  -w |  --wait \<time\> | Connect timeout |  
|  -z |  |  Zero-I/O mode, report connection status only |  

## Shutdown verzögert

Herunterfahren um c Minuten verzögern:
shutdown -r \<Minuten\>

Herunterfahren abbrechen:
shutdown -c

```bash
shutdown -r 10
shutdown -c
```

## Links Teaming und VLAN -

 [How to Configure Network Teaming in CentOS/RHEL 7](https://www.thegeekdiary.com/how-to-configure-network-teaming-in-centos-rhel-7/)

 [NIC Bonding & Teaming on CentOS 7](https://geekdudes.wordpress.com/2015/07/07/nic-bonding-teaming-on-centos-7/)

  [Configure Network Teaming in RHEL/CentOS 7](https://www.centlinux.com/2018/09/configure-network-teaming-rhel-centos-7.html)

  [Understanding Network Teaming](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/networking_guide/ch-configure_network_teaming)

```bash
nmcli connection show
nmcli device status

yum install teamd

```

```bash
# teamnl team0 ports
 3: enp0s8: up 1000Mbit FD
 2: enp0s3: up 1000Mbit FD
```

```bash
# teamdctl team0 state view
setup:
  runner: activebackup
ports:
  enp0s3
    link watches:
      link summary: up
      instance[link_watch_0]:
        name: ethtool
        link: up
        down count: 0
  enp0s8
    link watches:
      link summary: up
      instance[link_watch_0]:
        name: ethtool
        link: up
        down count: 0
runner:
  active port: enp0s3
```

```bash
# teamdctl team0 state view -v
setup:
  runner: activebackup
  kernel team mode: activebackup
  D-BUS enabled: yes
  ZeroMQ enabled: no
  debug level: 0
  daemonized: no
  PID: 741
  PID file: /var/run/teamd/team0.pid
ports:
  enp0s3
    ifindex: 2
    addr: 08:00:27:a3:ca:3b
    ethtool link: 1000mbit/fullduplex/up
    link watches:
      link summary: up
      instance[link_watch_0]:
        name: ethtool
        link: up
        down count: 0
        link up delay: 0
        link down delay: 0
  enp0s8
    ifindex: 3
    addr: 08:00:27:a3:ca:3b
    ethtool link: 1000mbit/fullduplex/up
    link watches:
      link summary: up
      instance[link_watch_0]:
        name: ethtool
        link: up
        down count: 0
        link up delay: 0
        link down delay: 0
runner:
  active port: enp0s3
```


## netplan

### Fixe IP Adresse

Im ordner Netplan das entsprechende File suchen.
z.B. /etc/netplan/00-installer-config.yaml

```text
# This is the network config written by 'subiquity'
network:
  version: 2
  ethernets:
    enp0s25:
      dhcp4: no
      addresses:
      - 192.168.1.132/24
      routes:
      - to: default
        via: 192.168.1.1
      nameservers:
        addresses: [8.8.8.8,8.8.4.4]
```

## Bonding

Anzeige des Status
```
cat /proc/net/bonding/bond0
```