---
title: Dokus
subtitle: Infos zu Delphi
comments: false
tags: [ "delphi"]
---

# Dokumentation

| Link | Beschreibung | 
|---|---|
|  [Delphi Alexandria Hauptseite](https://docwiki.embarcadero.com/RADStudio/Alexandria/de/Hauptseite) | Hilfe für RAD Studio 11.0 Alexandria |
|  [Delphi-Sprachreferenz](https://docwiki.embarcadero.com/RADStudio/Alexandria/de/Delphi-Sprachreferenz) | Sprachreferenz Delphi |
|  [Style Guide](https://docwiki.embarcadero.com/RADStudio/Sydney/en/Delphi’s_Object_Pascal_Style_Guide) | Delphi’s Object Pascal Style Guide |
|  [The Delphi Geek](https://www.thedelphigeek.com) | Projekte Primož Gabrijelčič |
|  [git repo](https://github.com/gabr42/RADStudio/Sydney/en/Delphi’s_Object_Pascal_Style_Guide) | Projekte Primož Gabrijelčič |