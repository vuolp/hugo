---
title: Cheatsheet
subtitle: Delphi Cheatsheet
comments: false
tags: [ "delphi"]
---

# Delphi Cheatsheet

## Tastenkürzel


| Tasten  | Funktion  |   |   |   |
|---|---|---|---|---|
|  [F12] |  Umstellen Code / Formular |   |   |   |
|  [Ctrl]+N | Neue Linie  |   |   |   |
|  [Ctrl]+ [+] | Grössere Zeichen  |   |   |   |
|  [Ctrl] + [-] | Kleiner Zeichen  |   |   |   |
|  [Ctrl] + [Shift] +[C] | Code Completion |   |   |   |
|  [Ctrl] + [Shift] +[T] | ToDo eintrag |   |   |   |
|  [Ctrl] + [Shift] +[Up] | Zur Definition |   |   |   |
|  [Ctrl] + [Shift] +[Down] | Zur Implementation |   |   |   |
|  [Alt] + [F11] | Unit verwenden |   |   |   |
|  [Ctrl] +[Alt] + [F12] | zu Unit wechseln |   |   |   |
|  [Ctrl] +[Alt] + [N][T] | Typen anzeigen|   |   |   |
|  [Ctrl] +[Alt] + [N][P] | Methoden anzeigen|   |   |   |
|  [Ctrl] +[J] | Temlates anzeigen|   |   |   |
|  [Ctrl] +[Space] | Auswahl verfügbare Methoden |   |   |   |
|  [Ctrl] +[Shift]+[Space] | Parameterliste anzeigen |   |   |   |
|  [Ctrl] +[W] | Wort beim Cursor auswählen |   |   |   |
|  [Ctrl] +[Shift]+[J] | Syncedit aufrufen |   |   |   |
|  [Ctrl] +[Shift]+[K][M] | Alle Methoden einfalten |   |   |   |
|  [Ctrl] +[Shift]+[K][T] | Einzelne Methoden einfalten |   |   |   |


## Tastenkürzel MMX
| Tasten  | Funktion  |   |   |   |
|---|---|---|---|---|
|  [Ctrl] + [Alt] +[Y] | Synchronisieren Deklaration / Implementation |   |   |   |
|  [Ctrl] + [L] | Lokale Variable basierend auf Cursorposition |   |   |   |
