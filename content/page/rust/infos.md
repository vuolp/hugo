---
title: Rust
subtitle: Interessante Links zu Rust
comments: false
tags: [ "rust"]
---
## Dokumentation

| Link | Beschreibung |
|---|---|
| [Rust Buch Deutsch](https://rust-lang-de.github.io/rustbook-de/)       | Rust Buch deutsch |
| [Rust deutsch](https://prev.rust-lang.org/de-DE/           )       | Rust deutsche Seite |
| [Rust by Example](https://doc.rust-lang.org/stable/rust-by-example/ ) | Rust by Example |
|  [Rust Reference](https://doc.rust-lang.org/reference/)               | Rust Referenz   |
|  [Rustplatz](https://www.rustplatz.de)                           | Inoffiziele deutsche Rust Seite |
|  [Video low level](https://www.youtube.com/watch?v=9_3krAQtD2k)        | Video Low Level |
|  [Video high level](https://www.youtube.com/watch?v=ThjvMReOXYM)        | Video High Level |

| Link | Beschreibung |
|---|---|
| [Wokwi](https://wokwi.com/rust) | Online Embedded Rust Simulator |
| [Git Repo](https://github.com/esp-rs) | Git Repo |
| [Embedded Rust documentation](https://docs.rust-embedded.org/) | Embedded Rust documentation |