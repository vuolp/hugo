---
title: Styleguide
date: 2023-10-04
tags: ["Styleguide"]
---

## Styleguide

> There are 2 hard problems in computer science:

cache invalidation, naming things, and off-by-1 errors

```python
number_of_donuts = 34
```

### kebab case

```python
number-of-donuts = 34
```

### camel case

```pascal
numberOfDonuts = 34
```

### pascal case

```pascal
NumberOfDonuts = 34
```
