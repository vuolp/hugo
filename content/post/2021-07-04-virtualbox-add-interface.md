---
title: Virtual Box zusätzliches Interfaces
date: 2021-07-04
tags: ["virtualbox"]
---

Zu den existierenden 4 Interfaces kann per Comandline ein weiteres Interface hinzugefügt werden.
Dazu ins Verzeichnis der Virtualbox wechseln und z.B. ein 5. Interface hinzufügen:

```powershell
cd C:\Program Files\Oracle\VirtualBox
.\VBoxManage modifyvm "Firewall" --hostonlyadapter5 Client
.\VBoxManage modifyvm "Firewall" --nic5 natnetwork
.\VBoxManage modifyvm "Firewall" --nictype5 82540EM
.\VBoxManage modifyvm "Firewall" --macaddress5 auto
.\VBoxManage modifyvm "Firewall" --cableconnected5 on
.\VBoxManage modifyvm "Firewall"--nat-network5 ClientNetwork
```
