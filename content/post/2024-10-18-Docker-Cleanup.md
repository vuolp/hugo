---
title: Docker Cleanup
date: 2022-03-06
tags: ["docker","cleanup"]
---

Saubermachen bei Dockers

```
# alle nicht referenzierten
docker system prune

# alle nicht benutzten
docker system prune -a
```
